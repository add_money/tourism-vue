/**
 * 设置登录信息
 * @param data
 */
export function setUserInfo(data) {
    localStorage.setItem('token', data.token)
    localStorage.setItem('category', data.user.category)
    localStorage.setItem('account', data.user.account)
    localStorage.setItem('userName', data.user.userName)
    localStorage.setItem('email', data.user.email)
    localStorage.setItem('telephone', data.user.telephone)
    localStorage.setItem('avatar', data.user.avatar)
    localStorage.setItem('userId', data.user.userId)
}

/**
 * 删除登录信息
 */
export function removeUserInfo() {
    localStorage.removeItem('token')
    localStorage.removeItem('category')
    localStorage.removeItem('account')
    localStorage.removeItem('userName')
    localStorage.removeItem('email')
    localStorage.removeItem('telephone')
    localStorage.removeItem('avatar')
    localStorage.removeItem('userId')
}

export const getStatusDescription = (status) => {
    switch (status) {
        case 1:
            return '待审核';
        case 2:
            return '审核通过';
        case 3:
            return '删除';
        case 4:
            return '注销';
        case 5:
            return '结束';
        default:
            return '未知状态';
    }
}

/**
 * ORDER: 订单,
 * STRATEGY: 攻略,
 * ADVERTISING: 广告,
 * TICKET: 门票,
 * SNACK: 小吃,
 * ATTRACTION: 景点,
 * HOTEL: 宾馆
 * @type {{ORDER: number, STRATEGY: number, ADVERTISING: number, TICKET: number, SNACK: number, ATTRACTION: number, HOTEL: number}}
 */
export const RELATION_TYPE = {
    HOTEL: 1,
    ADVERTISING: 2,
    ATTRACTION: 3,
    SNACK: 4,
    ORDER: 5,
    STRATEGY: 6,
    TICKET: 7,
}

import {getImageData} from "@/api/getImageData"

/**
 * 获取Logo图片
 * @param relationId
 * @param relationType
 * @param data
 * @param result
 */
export async function getLogo(relationId, relationType, data, result) {
    const res = await getImageData(
        {
            relationId,
            relationType,
            logoFlag: true
        }
    );
    if (res.code == 200) {
        let hotelList = [];
        hotelList = res.data;
        if (hotelList.length) {
            data.imageLogo = hotelList[0].imagePath;
            result.push(data)
        }
    } else {
        result.push(data);
    }
    return undefined;
}

/**
 * 获取多个图片信息
 * @param relationId
 * @param relationType
 * @param data
 * @param result
 */
export async function getImages(relationId, relationType, data, result) {
    const res = await getImageData(
        {
            relationId,
            relationType,
            logoFlag: false
        }
    );
    let param = []
    if (res.code == 200) {
        const hotelList = res.data;
        if (hotelList) {
            hotelList.forEach(item => {
                param.push(item.imagePath)
            })
        }
    }
    data.imageLogo = param;
    result.push(data);
}

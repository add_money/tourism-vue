import fetch from '@/config/fetch'

/**
 * 登陆
 */
export const login = data => fetch('/login', data, 'POST');

/**
 * 注册
 */
export const register = data => fetch('/tourism-user/register', data, 'POST');

/**
 * 修改密码
 */
export const changePwd = data => fetch('/tourism-user/password', data, 'POST');

/**
 * 头像上传
 */
export const avatarUpload = data => fetch('/tourism-user/avatar/upload', data, 'POST');

/**
 * 推荐标签初始化获取数据
 */
export const tagsInit = data => fetch('/tourism-tag/list', data, 'GET');

/**
 * 退出
 */
export const signout = (date) => fetch('/login-out?account=' + date);

/**
 * 获取用户信息
 */
export const getAdminInfo = (data) => fetch('/tourism-user/' + data);

/**
 * 获取当前用户银行卡信息
 */
export const accountMe = () => fetch('/tourism-bank-account/me');

/**
 * 当前用户银行卡信息新增
 */
export const accountAdd = (data) => fetch('/tourism-bank-account', data, 'POST');

/**
 * 当前用户银行卡信息修改
 */
export const accountUpdate = (data) => fetch('/tourism-bank-account/update', data, 'POST');

/**
 * api请求量
 */
export const apiCount = date => fetch('/statis/api/' + date + '/count');

/**
 * 所有api请求量
 */
export const apiAllCount = () => fetch('/statis/api/count');


/**
 * 所有api请求信息
 */
export const apiAllRecord = () => fetch('/statis/api/all');

/**
 * 用户注册量
 */
export const userCount = date => fetch('/statis/user/' + date + '/count');

/**
 * 某一天订单数量
 */
export const orderCount = date => fetch('/statis/order/' + date + '/count');


/**
 * 某一天管理员注册量
 */
export const adminDayCount = date => fetch('/statis/admin/' + date + '/count');

/**
 * 管理员列表
 */
export const adminList = data => fetch('/admin/all', data);

/**
 * 管理员数量
 */

export const adminCount = () => fetch('/admin/count');

/**
 * 获取定位城市
 */

export const cityGuess = () => fetch('/v1/cities', {
	type: 'guess'
});

/**
 * 添加商铺
 */

export const addShop = data => fetch('/shopping/addShop', data, 'POST');

/**
 * 获取搜索地址
 */

export const searchplace = (cityid, value) => fetch('/v1/pois', {
	type: 'search',
	city_id: cityid,
	keyword: value
});

/**
 * 广告列表
 * @param data
 */
export const advertisingList = (data) => fetch('/tourism-advertising/list', data);
/**
 * 广告审批通过
 * @param data
 */
export const advertisingAuditPassed = (data) => fetch('/tourism-advertising/audit-passed', data,'POST');
/**
 * 广告审批不通过
 * @param data
 */
export const advertisingAuditFailed = (data) => fetch('/tourism-advertising/audit-failed', data,'POST');
/**
 * 新增广告
 * @param data
 */
export const addAdvertising = (data) => fetch('/tourism-advertising', data,'POST');

/**
 * 旅游景点列表
 * @param data
 */
export const attractionList = (data) => fetch('/tourism-attraction/list', data);

/**
 * 旅游景点审核通过
 * @param data
 */
export const attractionAuditPassed = (data) => fetch('/tourism-attraction/audit-passed', data, 'POST');

/**
 * 审核不通过
 * @param data
 */
export const attractionAuditFailed = (data) => fetch('/tourism-attraction/audit-failed', data, 'POST');
/**
 * 新增游记
 * @param data
 */
export const addAttractionStrategy = (data) => fetch('/tourism-strategy', data, 'POST');

/**
 * 新增旅游景点
 * @param data
 */
export const addAttraction = (data) => fetch('/tourism-attraction', data, 'POST');
/**
 * 宾馆酒店列表
 * @param data
 */
export const hotelList = (data) => fetch('/tourism-hotel/list', data);
/**
 * 新增酒店
 * @param data
 */
export const addHotel = (data) => fetch('/tourism-hotel', data,'POST');

/**
 * 酒店审批通过
 * @param data
 */
export const hotelAuditPassed = (data) => fetch('/tourism-hotel/audit-passed', data,'POST');
/**
 * 删除宾馆
 * @param data
 */
export const deleteHotel = (data) => fetch('/tourism-hotel/'+data, {},'DELETE');
/**
 * 删除广告
 * @param data
 */
export const deleteAdvertising = (data) => fetch('/tourism-advertising/'+data, {},'DELETE');
/**
 * 删除景点
 * @param data
 */
export const deleteAttraction = (data) => fetch('/tourism-attraction/'+data, {},'DELETE');

/**
 * 获取用户信息
 * @param data
 */
export const getUserList = (data) => fetch('/tourism-user/list', {});
export const deleteUser = (data) => fetch('/tourism-user/'+data,data,'DELETE' );


/**
 * 酒店审批不通过
 * @param data
 */
export const hotelAuditFailed = (data) => fetch('/tourism-hotel/audit-failed', data,'POST');
/**
 * 新增小吃数据
 * @param data
 */
export const addTourismSnack = (data) => fetch('/tourism-snack', data,'POST');
export const delTourismSnack = (data) => fetch('/tourism-snack/'+data, {},'DELETE');
/**
 * 小吃
 * @param data
 */
export const snackList = (data) => fetch('/tourism-snack/list', data);

/**
 * 获取当前店铺食品种类
 */

export const getCategory = restaurant_id => fetch('/shopping/getcategory/' + restaurant_id);

/**
 * 添加食品种类
 */

export const addCategory = data => fetch('/shopping/addcategory', data, 'POST');


/**
 * 添加食品
 */

export const addFood = data => fetch('/shopping/addfood', data, 'POST');


/**
 * category 种类列表
 */

export const foodCategory = (latitude, longitude) => fetch('/shopping/v2/restaurant/category');

/**
 * 获取餐馆列表
 */

export const getResturants = data => fetch('/shopping/restaurants', data);

/**
 * 获取餐馆详细信息
 */

export const getResturantDetail = restaurant_id => fetch('/shopping/restaurant/' + restaurant_id);

/**
 * 获取餐馆数量
 */

export const getResturantsCount = () => fetch('/shopping/restaurants/count');

/**
 * 更新餐馆信息
 */

export const updateResturant = data => fetch('/shopping/updateshop', data, 'POST');

/**
 * 删除餐馆
 */

export const deleteResturant = restaurant_id => fetch('/shopping/restaurant/' + restaurant_id, {}, 'DELETE');

/**
 * 获取食品列表
 */

export const getFoods = data => fetch('/shopping/v2/foods', data);

/**
 * 获取食品数量
 */

export const getFoodsCount = data => fetch('/shopping/v2/foods/count', data);


/**
 * 获取menu列表
 */

export const getMenu = data => fetch('/shopping/v2/menu', data);

/**
 * 获取menu详情
 */

export const getMenuById = category_id => fetch('/shopping/v2/menu/' + category_id);

/**
 * 更新食品信息
 */

export const updateFood = data => fetch('/shopping/v2/updatefood', data, 'POST');

/**
 * 删除食品
 */

export const deleteFood = food_id => fetch('/shopping/v2/food/' + food_id, {}, 'DELETE');

/**
 * 获取用户数量
 */

export const getUserCount = data => fetch('/v1/users/count', data);

/**
 * 获取订单列表
 */

export const getOrderList = data => fetch('/bos/orders', data);

/**
 * 获取订单数量
 */

export const getOrderCount = data => fetch('/bos/orders/count', data);

/**
 * 获取用户信息
 */

export const getUserInfo = user_id => fetch('/v1/user/' + user_id);

/**
 * 获取地址信息
 */

export const getAddressById = address_id => fetch('/v1/addresse/' + address_id);

/**
 * 获取用户分布信息
 */

export const getUserCity = () => fetch('/v1/user/city/count');


/**
 * 攻略列表
 */
export const getStrategyList = data => fetch('/tourism-strategy/list', data);

/**
 * 添加攻略
 */
 export const newStrategy = data => fetch('/tourism-strategy', data, 'POST');

 /**
  * 审核通过攻略
  */
 export const auditStrategy = data => fetch('/tourism-strategy/audit-passed', data, 'POST');

 /**
 * 删除攻略
 */
  export const deleteStrategy = data => fetch('/tourism-strategy/' + data, {}, 'DELETE');

/**
 * 攻略详情
 */
  export const strategyDetail = data => fetch('/tourism-strategy/' + data);

/**
 * 攻略评论列表
 */
  export const commentList = data => fetch('/tourism-reply/list', data);

/**
 * 路线列表
 */
  export const straAttractionRelList = data => fetch('/tourism-stra-attraction-rel/list', data);

/**
 * 删除路线
 */
 export const deleteStraAttraction = data => fetch('/tourism-stra-attraction-rel/' + data, {}, 'DELETE');

/**
 * 删除评论
 */
export const deleteComment = data => fetch('/tourism-reply/' + data, {}, 'DELETE');

export const recommendation = data => fetch('/tourism-attraction/recommendation', {}, 'GET');

/**
 * 请求图片
 * @param data
 */
export const tourismImage = data => fetch('/tourism-image/'+data, {}, 'GET');

/**
 * 旅游景点详情
 * @param data
 */
export const tourismAttractionInfo = data => fetch('/tourism-attraction/'+data, {}, 'GET');

/**
 * 订单新增
 * @param data
 */
export const tourismOrderAdd = data => fetch('/tourism-order-header',data, 'POST');
/**
 * 支付
 * @param data
 */
export const tourismOrderPay = data => fetch('/tourism-order-header/pay/'+data,{}, 'PUT');

/**
 * 游记
 * @param data
 */
export const tourismStrategy = data => fetch('/tourism-strategy/list?current=1&size=1000',{}, 'GET');

/**
 * 游记评价
 * @param data
 */
export const tourismReply = data => fetch('/tourism-reply/list',data, 'GET');
/**
 * 新增评论
 * @param data
 */
export const addTourismReply = data => fetch('/tourism-reply',data, 'POST');

/**
 * 游记详情
 * @param data
 */
export const tourismStrategyDetail = data => fetch('/tourism-strategy/'+data, {}, 'GET');

/**
 *
 * @param data
 */
export const tourismOrderList = data => fetch('/tourism-acc-order-rel/list', {}, 'GET');






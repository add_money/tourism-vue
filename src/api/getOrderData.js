import fetch from '@/config/fetch'

/**
 * 获取当前人订单列表
 * @param data
 */
export const getOrderHeaderInfo = data => fetch('/tourism-order-header/list', data);

/**
 * 删除当前人订单
 * @param data
 */
export const deleteOrderHeader = orderHeaderId => fetch('/tourism-order-header/' + orderHeaderId, {}, 'DELETE');

/**
 * 管理员获取所有订单列表
 * @param data
 */
export const getOrderHeaderInfoAdmin = data => fetch('/tourism-order-header/list-admin', data);

/**
 * 订单头详细信息
 * @param data
 */
export const getOrderHeaderById = orderHeaderId => fetch('/tourism-order-header/list-admin', orderHeaderId);

/**
 * 根据订单头ID拿到订单行数据
 * @param data
 */
export const getOrderLinesById = orderHeaderId => fetch('/tourism-order-line/list?orderHeaderId='+orderHeaderId);

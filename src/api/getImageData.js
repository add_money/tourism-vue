import fetch from '@/config/fetch'

/**
 * 获取当前人订单列表
 * @param data
 */
export const getImageData = data => fetch('/tourism-image/list', data);

/**
 * 上传图片
 * @param data
 */
export const uploadImage = data => fetch('/tourism-image/upload', data, 'POST');

import fetch from '@/config/fetch'

/**
 * 获取宾馆列表
 * @param data
 */
export const getSnackList = data => fetch('/tourism-snack/list', data);

/**
 * 获取宾馆详情
 * @param data
 */
export const getSnackInfo = data => fetch('/tourism-snack/' + data, {}, 'GET');


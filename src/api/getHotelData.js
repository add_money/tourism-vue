import fetch from '@/config/fetch'

/**
 * 获取宾馆列表
 * @param data
 */
export const getHotelList = data => fetch('/tourism-hotel/list', data);

/**
 * 获取宾馆详情
 * @param data
 */
export const getHotelInfo = data => fetch('/tourism-hotel/' + data, {}, 'GET');


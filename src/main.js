import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store/'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import mavonEditor from 'mavon-editor'
import 'mavon-editor/dist/css/index.css'
import VModal from 'vue-js-modal'

//使用
Vue.use(mavonEditor)

Vue.config.productionTip = false;

Vue.use(ElementUI);

new Vue({
    el: '#app',
    router,
    store,
    template: '<App/>',
    components: {App}
})

Vue.use(VModal, {dynamic: true, injectModalsContainer: true, dynamicDefaults: {clickToClose: true}});


import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const login = r => require.ensure([], () => r(require('@/page/login')), 'login');
const register = r => require.ensure([], () => r(require('@/page/register')), 'register');
const manage = r => require.ensure([], () => r(require('@/page/manage')), 'manage');
const home = r => require.ensure([], () => r(require('@/page/home')), 'home');
const addShop = r => require.ensure([], () => r(require('@/page/addShop')), 'addShop');
const addGoods = r => require.ensure([], () => r(require('@/page/addGoods')), 'addGoods');
const userList = r => require.ensure([], () => r(require('@/page/userList')), 'userList');

const shopList = r => require.ensure([], () => r(require('@/page/shopList')), 'shopList');
const foodList = r => require.ensure([], () => r(require('@/page/foodList')), 'foodList');
const orderHeaderList = r => require.ensure([], () => r(require('@/page/orderHeaderList')), 'orderHeaderList');
const orderInfo = r => require.ensure([], () => r(require('@/page/orderInfo')), 'orderInfo');
const adminList = r => require.ensure([], () => r(require('@/page/adminList')), 'adminList');
const visitor = r => require.ensure([], () => r(require('@/page/visitor')), 'visitor');
const newMember = r => require.ensure([], () => r(require('@/page/newMember')), 'newMember');
const uploadImg = r => require.ensure([], () => r(require('@/page/uploadImg')), 'uploadImg');
const vueEdit = r => require.ensure([], () => r(require('@/page/vueEdit')), 'vueEdit');
const adminSet = r => require.ensure([], () => r(require('@/page/adminSet')), 'adminSet');
const sendMessage = r => require.ensure([], () => r(require('@/page/sendMessage')), 'sendMessage');
const explain = r => require.ensure([], () => r(require('@/page/explain')), 'explain');

const newStrategy = r => require.ensure([], () => r(require('@/page/newStrategy')), 'newStrategy');
const strategyList = r => require.ensure([], () => r(require('@/page/strategyList')), 'strategyList');
const strategyDetail = r => require.ensure([], () => r(require('@/page/strategyDetail')), 'strategyDetail');
const advertisingPage = r => require.ensure([], () => r(require('@/page/advertising')), 'advertisingPage');
const attractionPage = r => require.ensure([], () => r(require('@/page/attraction')), 'attractionPage');
const tourismHotel = r => require.ensure([], () => r(require('@/page/tourism-hotel')), 'tourismHotel');
const tourismSnack = r => require.ensure([], () => r(require('@/page/tourism-snack')), 'tourismSnack');
const straAttractionRelList = r => require.ensure([], () => r(require('@/page/straAttractionRelList')), 'straAttractionRelList');
const commentList = r => require.ensure([], () => r(require('@/page/commentList')), 'commentList');
const homePage = r => require.ensure([], () => r(require('@/page/home-page')), 'home');
const homeHotel = r => require.ensure([], () => r(require('@/page/home-hotel')), 'homeHotel');
const homeHotelInfo = r => require.ensure([], () => r(require('@/page/home-hotel-info')), 'homeHotelInfo');
const homeSnack = r => require.ensure([], () => r(require('@/page/home-snack')), 'homeSnack');
const homeSnackInfo = r => require.ensure([], () => r(require('@/page/home-snack-info')), 'homeSnackInfo');
const homeAttractionInfo = r => require.ensure([], () => r(require('@/page/home-attraction-info')), 'homeAttractionInfo');
const homeAttractionList = r => require.ensure([], () => r(require('@/page/home-attraction-list')), 'homeAttractionList');
const homeTravels = r => require.ensure([], () => r(require('@/page/home-travels')), 'homeTravels');
const homeTravelsInfo = r => require.ensure([], () => r(require('@/page/home-travels-info')), 'homeTravelsInfo');
const homeOrder = r => require.ensure([], () => r(require('@/page/home-order')), 'homeOrder');
const homeOrderInfo = r => require.ensure([], () => r(require('@/page/home-order-info')), 'homeOrderInfo');

const routes = [
	{
        path: '/',
        component: homePage
	},{
        path: '/login',
        component: login
    },{
        path: '/homeHotel',
        component: homeHotel
    },{
        path: '/homeTravels',
        component: homeTravels
    },{
        path: '/homeOrder',
        component: homeOrder
    },{
        path: '/homeOrderInfo',
        component: homeOrderInfo
    },{
        path: '/homeTravelsInfo',
        component: homeTravelsInfo
    },{
        path: '/homeHotelInfo',
        component: homeHotelInfo
    },{
        path: '/homeAttractionInfo',
        component: homeAttractionInfo
    },{
        path: '/homeSnack',
        component: homeSnack
    },{
        path: '/homeSnackInfo',
        component: homeSnackInfo
    },{
        path: '/homeAttractionList',
        component: homeAttractionList
    },
    {
        path: '/register',
        component: register
    },
	{
		path: '/manage',
		component: manage,
		name: '',
		children: [{
			path: '',
			component: home,
			meta: [],
		},{
			path: '/addStrategy',
			component: addShop,
			meta: ['添加数据', '添加攻略'],
		},{
			path: '/addGoods',
			component: addGoods,
			meta: ['添加数据', '添加商品'],
		},{
			path: '/userList',
			component: userList,
			meta: ['数据管理', '用户列表'],
		},{
			path: '/shopList',
			component: shopList,
			meta: ['数据管理', '商家列表'],
		},{
			path: '/foodList',
			component: foodList,
			meta: ['数据管理', '食品列表'],
		},{
			path: '/orderHeaderList',
			component: orderHeaderList,
			meta: ['数据管理', '订单列表'],
		},{
            path: '/orderInfo',
            component: orderInfo,
            meta: ['数据管理', '订单详情'],
        },
            {
			path: '/adminList',
			component: adminList,
			meta: ['数据管理', '管理员列表'],
		},{
			path: '/visitor',
			component: visitor,
			meta: ['图表', '用户分布'],
		},{
			path: '/newMember',
			component: newMember,
			meta: ['图表', '用户数据'],
		},{
			path: '/uploadImg',
			component: uploadImg,
            meta: ['数据管理', '功能图片上传']
		},{
			path: '/vueEdit',
			component: vueEdit,
			meta: ['编辑', '文本编辑'],
		},{
			path: '/adminSet',
			component: adminSet,
			meta: ['设置', '管理员设置'],
		},{
			path: '/sendMessage',
			component: sendMessage,
			meta: ['设置', '发送通知'],
		},{
			path: '/explain',
			component: explain,
			meta: ['说明', '说明'],
		},
        {
			path: '/strategyList',
			component: strategyList,
			meta: ['数据管理', '攻略列表'],
		},
        {
			path: '/newStrategy',
			component: newStrategy,
			meta: ['添加数据', '添加攻略'],
		},
        {
            path: '/strategyDetail',
            component: strategyDetail,
			meta: ['数据列表', '攻略详情'],

            },
            {
                path: '/tourismHotel',
                component: tourismHotel,
                meta: ['数据管理', '宾馆管理'],

            }
            ,{
                path: '/tourismSnack',
                component: tourismSnack,
                meta: ['数据管理', '小吃管理'],

            }
            ,
            {
                path: '/advertisingPage',
                component: advertisingPage,
                meta: ['数据管理', '广告管理'],

            }
            ,{
                path: '/attractionPage',
                component: attractionPage,
                meta: ['数据管理', '旅游景点管理'],

            },
            {
                path: '/straAttractionRelList',
                component: straAttractionRelList,
                meta: ['数据列表', '路线管理'],

            }, {
                path: '/commentList',
                component: commentList,
                meta: ['数据列表', '评论管理'],
            }]
    }
]

export default new Router({
    routes,
    strict: process.env.NODE_ENV !== 'production',
})
